Master Services Agreement

1
Scope Of Work, Additional Services, Fees

1.1
Scope of Work. Provider shall perform the services described in the Scope of Work attached as Exhibit A. Provider shall deliver any work product according to the schedule on Exhibit A unless delivery is affected by factors beyond the control of Provider.

1.2
Amendments to the Scope of Work. Client may request amendments to the Scope of Work. Upon receiving a request to amend the Scope of Work, within five business days, Provider shall provide Client:
(a) The time necessary to make the requested change.
(b) An estimate of additional fees, costs, or expenses that may be charged as a result of the requested change.
(c) A description of how the requested change may affect other services under the Scope of Work, if any.
(d) A description of any changes to this Agreement that may be required by the requested change.
(e) The parties must agree in writing to any amendments to the Scope of Work before Provider will perform the amendment to the Scope of Work.

1.3
Project Services. Client acknowledges that any requested services not included in the Scope of Work will be treated as Project Services. Provider will assess fees, costs, and expenses for Project Services according to Provider’ billing rates in effect at the time the Project Services begin. Provider may request Client to sign a separate agreement for Project Services.

1.4
Service to Hardware After Criminal Acts or Viruses. All services by Provider necessary to repair, rebuild, or replace Hardware or Software after criminal activity or a virus has damages Client’s Hardware, Software, data, or network will be billed separate as Project Services.

1.5
Adjustment to Service Fees Under Scope of Work. Provider may adjust fees stated in the Scope of Work if:
(a) the number of Users exceeds the number stated in the Scope of Work.
(b) the number, type, and location of Client’s Hardware exceeds what is stated in the Scope of Work.
(c) this Agreement auto-renews as long as Provider gives at least 60 days written notice of the increase in fees.

1.6
Third Party Surcharges.
Provider may charge Client for any third-party surcharges assessed by governmental, quasi-governmental, or other third parties related to the services under the Scope of Work or Project Services. Third Party Surcharges do not include any sales taxes assessed on the services, Hardware, or Software.

2
Invoicing And Payment

2.1
Invoices. Provider shall issue invoices monthly or upon completion of the Scope of Work. Provider will list all fees, costs, expenses, and any taxes on an invoice. Provider may issue a separate invoice for Project Services.

2.2
Payments by Client. Client shall pay all invoices within five business days of the date of the invoice.

2.3
Costs and Expenses. Costs and expenses may include but are not limited to printing, fabrication, postage, commercial delivery services, shipping and packaging, travel costs, or any other necessary cost or expense incurred to perform the Scope of Work or Project Services. Provider will itemize costs and expenses on the next invoice after incurring the cost or expense. Provider may request Client to make an advance deposit to pay for costs and expenses.

2.4
Emergency Action Fees. If Client requires immediate or emergency services during non-working hours not caused by Provider’ action or inaction, Provider may charge emergency action fees. Emergency Action Fees apply during all United States Holidays, Saturdays, Sundays, or anytime between 7:00 p.m. Pacific Time through 8:00 a.m. Pacific Time. Emergency Action Fees are $225.00 per hour and have a three-hour minimum charge.

2.5
Late Fee, Interest, and Collections. If Client fails to pay invoices on time, then the following provisions apply.

Days After Invoice Date > Financial Penalty > Stop Work Action
10 Days > $75 late fee per day starting Day 10 > Stop Work on Project Services
30 Days > 2% interest on unpaid amount every 30 days > Stop Work on All Services, except emergencies
60 Days > Refer to Collections, Client to pay all reasonable collection fees and expenses > Stop All Work

2.6
Taxes. Fees, costs, and expenses are stated without taxes included. Client shall pay all sales, use, gross receipts, value added, or any other taxes that may be assessed by any federal, state, or local taxing authority.

3
Client Information Technology Environment Standards

3.1
Minimum Standards. Before Provider provides services under the Scope of Work, Client states that Client’s Information Technology Environment meets the minimum standards stated here.

3.2
Windows Server. Client’s technology with Microsoft Operating Systems uses Windows 2012 Server or later, and have all the latest Microsoft Service Packs and Critical Updates installed

3.3
Desktops and Notebooks/Laptops. All Desktop PC’s and Notebooks/Laptops with Microsoft Windows Operating Systems uses Windows 10 Pro with all the latest Microsoft Service Packs and Critical Updates installed. All Apple equipment uses the latest manufacturers operating system or the operating system version immediately prior to the current version.

3.4
Handheld Devices. All handheld devices use the latest manufacturers operating system or the operating system version immediately prior to the current version.

3.5
Client Software. All Server, Desktop, and Notebook/Laptop software is genuine, currently licensed, and Vendor-Supported.

3.6
Anti-Virus Protection. The environment has a currently licensed, up-to-date and Vendor-Supported server-based antivirus solution protecting all servers, desktop, notebooks/laptops, and email.

3.7
Backup Solution. The Client environment has a currently licensed, Vendor-Supported server-based backup solution that can be monitored and sends notifications on job failures and successes.

3.8
Hardware Firewall. The Client environment has a currently licensed, Vendor-Supported hardware firewall between the Internal Network and the Internet.

3.9
Wireless Encryption. All wireless data traffic in the environment is encrypted.

3.10
Additional Costs. Any labor or costs incurred by Provider to bring Client Information Technology Environment to the above listed standards will be charged as Project Services.

4
Client Statements

4.1
No Conflicts. The execution of this agreement does not conflict with or violate any other contract to which the Client is bound.

4.2
Licenses and Consents. The Client will maintain all licenses or consents necessary to operate under this agreement.

4.3
Compliance with Laws, Investigations. The Client will comply with all applicable laws and regulations and assist with all reasonable private and legal investigations.

4.4
Access. Client shall provide Provider reasonable and timely access to the Client's premises, network, data, and other facilities.

4.5
Requested Information. Client shall provide materially accurate information reasonably requested by Provider.

4.6
Client Premises. Client has prepared its premises, data, and other facilities for Provider access.

4.7
Notice to Third Parties. Client has notified all third parties whose services may be impacted by or whose services impact Provider’ services.

4.8
Third-Party Terms of Service. Client shall comply with all third-party Terms of Service for Hardware, Software, or Services related to Client’s Information Technology Environment.

4.9
Internet Connectivity Risks. Client acknowledges the risks of connecting to the internet, including the risks of data loss, data corruption, temporary loss of internet availability, security breaches, or other malicious activity by third-parties not within the reasonable control of Provider.

4.10
Consultation with Provider. Client acknowledges that assent to activity by Provider after consultation also means assent to the risks of third-party services, Hardware, or Software. Other than in cases of emergency conditions that may carry risk of significant material damage to Client’s Hardware, Software, or data, Client acknowledges that assent to actions by Provider by Client also means that Client has been given sufficient opportunity to raise questions or concerns to Provider.

4.11
Payment of Ransoms. Client shall pay any ransoms demanded as the result of criminal behavior by third parties unless excepted by Section “Consultation with Provider”.

5
Provider Statements

5.1
No Conflicts. The execution of this agreement does not conflict with or violate any other contract to which the Client is bound.

5.2
Licenses and Consents. Provider will maintain all licenses or consents necessary to operate under this agreement.

5.3
Compliance with Laws, Investigations. The Client will comply with all applicable laws and regulations and assist with all reasonable private and legal investigations.

5.4
Standard of Care by Provider. Provider states that the personnel, including Subcontractors, assigned by Provider meet requisite competency consistent with the applicable industry standards to perform the services.

5.5
Reliance by Provider. Provider relies upon the statements of purpose provided by Client and makes no warranties as to the merchantability, quality, suitability, or usability of any work product or deliverable for any purpose other than the purpose communicated by Client to Provider.

5.6
Services Estimates. Provider states that estimates for amendments to the Scope of Work, Project Services or any other request for an estimate of fees are estimates only. Estimates may not be interpreted to be an upper limit on the charges assessed to Client.

5.7
Warranty of Safety, Security, Availability, Access and Effectiveness. Provider will use reasonable efforts, applying standards applicable to Provider’s industry, to ensure the security, safety, availability, access, and effectiveness of the Hardware and Software used by, recommended by, or implemented by Provider at Client’s direction. However, Provider makes no warranties or promises about security, safety, availability, or effectiveness of Hardware and Software due to action by persons, entities, or parties beyond the reasonable control of Provider.

5.8
Third Party Hardware and Software “As Is”. All Hardware or Software obtained by Provider on Client’s behalf or provided directly by third parties is provided “As Is.” Provider will make a reasonable inquiry into the efficacy of third-party Hardware and Software, but makes no warranties or promises about effectiveness of third party Hardware or Software beyond Client’s stated business requirements. Provider makes no warranties or promises about the fitness of any Hardware or Software for any purpose, use, or party not included in Client’s stated business requirements.

5.10
Consultation with Client, Exception. Provider may make recommendations to the Client about connecting to your using third-party services, Hardware, or Software. Provider will consult with Client before implementing any third-party services, Hardware, or Software unless emergency circumstances make consultation with the Client impossible or impractical without significant material damage to Client’s Hardware, Software, or data. Provider will make a reasonable inquiry into the material aspects and attributes of third-party services. Provider makes no warranties or promises of any kind about performance, security, accuracy or any changes of any kind of third party services after initial implementation by Provider.

6
Confidentiality

6.1
Duty to Maintain Confidentiality. Each party shall maintain the confidentiality of Confidential Information received from the other party in the same way the party maintains its Confidential Information, but in no circumstances less than commercially reasonable security.

6.2
Exceptions to Duty to Maintain Confidentiality The obligation not to use or disclose Confidential Information will remain in effect until one of these exceptions occurs. Information is not considered Confidential Information if the information is

(a) was known to the general public at the time of disclosure.

(b) was known to the recipient at the time of disclosure through permissible means.  

(c) is disclosed with the prior written approval of the party owning the Confidential Information.

(d) was independently developed by the recipient without any use of the Confidential Information of the disclosing party.

(e) becomes known to the recipient from a source other than the disclosing party without breach of this Agreement.

6.3
Nondisclosure. Neither party may disclose Confidential Information to any third party without the prior written approval of the party owning the Confidential Information. If disclosure is requested by a subpoena or other government order, the party receiving the disclosure request shall promptly notify the other party of the disclosure request. The disclosing party may seek a protective order and the recipient shall cooperate in the effort to obtain a protective order.

7
Limitations On Liability

7.1
Disclaimers of Liability by Provider. Provider makes the following disclaimers of liability in this article. Provider has no liability and may not held liable for any damage or loss of any kind or amount to the Client under any of the circumstances stated in this Section.

7.2
Disclaimer of Liability Due to Client Action or Inaction. Provider is not liable to Client due to Client’s action or inaction which affect Provider’ ability to provide services.

7.3
Client Requirements. Provider is not liable to Client due to Client’s willful or negligent failure to provide a reasonable description of Client’s operational requirements and expectations.

7.4
Client’s Failure to Consult. Provider is not liable to Client due to Client’s willful or negligent failure to consult with or obtain information from Provider regarding the implementation of third-party services, Hardware, or Software. If Client does not engage in reasonable consultation, Provider may use its reasonable business judgment to advance Client’s stated requirements and expectations. Client shall pay all fees and costs associated with correcting Provider’s actions under this Section if Client does not ratify Provider’ business judgement.

7.5
Disabling Security. Provider is not liable to Client due to Client’s willful or negligent actions to disable, bypass, or delete of any security, safety, firewall, or other technology designed and implemented to secure Client’s network and data.

7.6
Data Sharing. Provider is not liable to Client due to Client’s willful or negligent actions to share data or network access to third parties which results in loss or damage of any kind to Client’s operations, data, or network.

7.7
Criminal Activity by Third Parties. Provider is not liable to Client due to criminal activity by third parties which includes but is not limited to hacking, phishing, crypto-locking, installing ransomware, or any other activity that is currently illegal or made illegal during the term of this agreement.  that bypasses or hacks through the reasonable security Hardware or Software provided or recommended by Provider.

7.8
Viruses or Malware. Provider is not liable to Client due to financial losses, loss of data, corruptions of data or damage to Client’s network or Hardware caused by viruses or malware that result from Client’s access to third party data, networks, programs, services, or application programming interfaces (APIs). Viruses include but are not limited to, disabling viruses, drop-dead devices or attacks, time bombs, trap doors, trojan horses, worms, denial of services attacks, or any similar mechanism or attack.

7.9
Exception to Limits of Liability. Provider has no financial or other liability under this Section unless a group of at least three independent third-party experts determine Provider’ actions or failures to act to secure the Client’s Hardware, network, or data were unreasonable or contrary to applicable industry standards.

7.10
Limit of Provider’ Financial Liability. If a group of at least three experts determines Provider acted unreasonable, failed to act reasonably, or failed to apply applicable industry standards, then Provider’ liability shall be limited to the sum total of:
(a) maximum benefit permitted under Provider’ liability insurance if and only if paid by the insurance company according to its claims process;
(b) the fees paid by Client under this agreement;
(c) the reasonable costs to repair or replace Client’s Hardware and Software.

7.11
Indemnification by Client. Client shall indemnify Provider for all damages to Provider’s Hardware, Software, network or other customers resulting from Client’s willful or negligent actions that permit or provide an opportunity for viruses, malware, or other activity to gain access to Provider’s Hardware, Software, network, or other customers’ networks.

8
Relationship Of The Parties

8.1
Independent Contractor. Provider is an independent contractor, not an employee of Client or any company affiliated with Client. This Agreement does not create a partnership or joint venture of any kind. Neither party is authorized to act as agent or bind the other party except about the services described in this agreement.

8.2
Use of Subcontractors. Provider may engage Subcontractors to deliver any part of the Scope of Work. Provider is responsible for each Subcontractor’s compliance with terms of this Agreement. Unless agreed to in advance by Client, Provider will pay Subcontractors from Provider’ funds.

8.3
Non-Solicitation of Provider Employees.
(a) During the term of this Agreement, and for six months after termination of this agreement, Client shall not solicit or induce any Provider employee to leave Provider to be employed or contracted by Client whether or not the Provider employee has been assigned to perform tasks under this Agreement.
(b) Client acknowledges that Provider has expended significant time and expense to recruit, hire, and train Provider employees.
(c) If Client violates this section, Client shall pay Provider an agency commission equal to:
  (1) 35 percent of the former Provider employee’s starting annual salary (or starting hourly wage multiplied by 2000) with Client which payment is due within thirty days of the employee starting date or date which Provider discovers a violation of this Subsection; or
  (2) 35 percent of fees paid to the former Provider employee if engaged by Client as an independent contractor payable at the end of the calendar month during which the independent contractor performed services for Client or date which Provider discovers a violation of this Subsection.
  (3) If Provider does not receive the payments described in this Section, Provider may seek all remedies under law and equity without limit on compensation stated in this section.

8.4
Non-Solicitation of Provider Subcontractors.
(a) During the time of a subcontractor’s engagement by Provider, and for 60 days after the termination of the subcontractor, Client shall not to solicit, employ, or engage Provider Subcontractors to provide services related to the Scope of Work without the written consent of Provider.
(b) Client acknowledges that Provider has expended significant time and expense to identify, recruit, retain, and train Provider subcontractors.
(c) If Client violates this section, Client shall pay Provider an agency commission equal to:
  (1) 35 percent of the subcontractor’s starting annual salary (or starting hourly wage multiplied by 2000) with Client which payment is due within thirty days of the employee starting date or date which Provider discovers a violation of this Subsection; or
  (2) 35 percent of fees paid to the subcontractor if engaged by Client as an independent contractor payable at the end of the calendar month during which the independent contractor performed services for Client or date which Provider discovers a violation of this Subsection.
  (3) If Provider does not receive the payments described in this Section, Provider may seek all remedies under law and equity without limit on compensation stated in this section.

8.5
No Exclusivity. The parties acknowledge that this agreement does not create an exclusive relationship between the parties. Except as limited by the Non-Solicitation Sections, Client may engage others to perform services of the same or similar nature to those provided by Provider, and Provider may offer and provide services to other persons or entities.

9
Term And Termination

9.1
Term of Agreement. The term of this agreement is two years.

9.2
Renewal. This agreement will automatically renew for an additional term of two years unless one party gives at least 60 days written notice of intent to not renew this agreement.

9.3
Termination. This agreement may be terminated by:
(a) mutual consent of the parties in writing.
(b) Client without cause by giving 60 days written notice to Provider.
(c) Client after receiving notice of a fee increase from Provider.
(d) Provider without cause by giving 60 days written notice to Client.
(e) Provider with cause if Client has not materially complied with Client’s duties under this agreement and Client does not correct the failures after notice from Provider. Provider shall give Client written notice of Client’s material failures. Provider may terminate this agreement under this subsection if and only if Client does not correct its failures to the reasonable satisfaction of Provider within 30 days of the notice.

(f) Client with cause if Provider has not materially complied with Provider’s duties under this agreement and Provider does not correct the failures after notice from Client. Client shall give Provider written notice of Provider’s material failures. Client may terminate this agreement under this subsection if and only if Provider does not correct its failures to the reasonable satisfaction of Client within 30 days of the notice.

9.4
Payments Due on Termination. If this agreement is terminated by the Client without cause, the Client shall pay Provider for the remaining months in the term as a termination fee. The termination fee is the average of the previous six months of invoices under the Scope of Work. Project Services are not included in the termination fee calculation.

9.5
Actions Incident to Termination. If this agreement is terminated for any reason, then within 15 days, the parties shall:
(a) Return all of the other party’s Confidential Information.
(b) Remove all Hardware or Software belonging to the other party.

10
Miscellaneous Terms

10.1
Entire Agreement. This agreement states the entire understanding between the parties regarding the subject matter of this agreement. Unless specifically included as part of this agreement, this agreement supersedes and replaces all previous agreements, proposals, estimates, warranties, representations, or communications between the parties, whether oral or written regarding the subject matter of this agreement.

10.2
Governing Law. The laws of the State of California govern all adversarial proceedings related to this agreement, without giving effect to its principals of conflicts of law. The parties shall file and conduct any dispute resolution proceeding in Santa Clara County, California. The parties submit to the jurisdiction of Santa Clara County courts.

10.3
Notices.

(a) For a notice required by this agreement to be valid, it must be in writing and delivered (1) by hand, (2) by a national transportation company, with all fees prepaid, (3) registered or certified mail, return receipt requested and postage prepaid, or (4) by electronic mail with a return receipt notification.
(b) Subject to subsection (c), a valid notice agreement will be effective when received by the party to which it is addressed. It will be deemed to have been received as follows:
  (1) if the notice is delivered by hand, a national transportation company or delivered by mail, upon receipt as indicated by the date on a signed receipt;
  (2) if the notice is delivered by email, upon receipt as indicated in the automatic return receipt email generated; and
  (3) if the party to which it is addressed rejects or refuses to accept it, or if it cannot be delivered because of a change in address or in the email address for which no notice was given, then upon that rejection, refusal, or inability to deliver.
(c) If a notice addressed to a party is received after 5:00 p.m. on a business day at the proper address, or on a non-business day, then the notice will be deemed delivered at 9:00 a.m. on the next business day.
(d) For a notice or other communication to a party under this agreement to be valid, it must be addressed using the information specified below for that party or any other information specified by that party in a notice in accordance with this Section.

To: Providers

BACS, Inc.
Attn: Michael Kushner
1930 Old Middlfield Way, Suite B
Mountain View, CA 94043
Telephone: (650) 887-4601

If to Client:

Rhodes Development Company
ATTN: Gene Rhodes
1401 Avocado Avenue, Suite 901
Newport Beach, CA 92660
Telephone: (949) 644-0603

10.4
Force Majeure.
(a) A party will not be in breach of this agreement or liable for any delay or inability to perform under this agreement if the delay or inability to perform is related to a Force Majeure Event.
(b) If a party experiences a Force Majeure Event, that party shall promptly notify the other party of the event describe the event, an estimated delay caused by the Force Majeure Event, and any steps the party is taking to mitigate the Force Majeure Event.
(c) The parties shall consult within a reasonable time to discuss suspending or delaying performance by all parties, terminating the agreement if appropriate, or other steps to mitigate the effects of the Force Majeure Event.
(d) The declaration of a Force Majeure Event does not relieve the parties for liability, payment, or delivery of goods or services prior to the Force Majeure Event.

10.5
Assignment of Agreement. Neither party may transfer this Agreement without the prior written consent of the other party.

10.6
Severability. The partied intend as follows:
(a) that if any provision of this agreement is deemed unenforceable, then that provision will be modified only so much to make it enforceable unless that modification is not permitted by law, in which case that provision will be ignored.
(b) that if an unenforceable provision is modified or disregarded by this Section, then the rest of the agreement remains effective; and
(c) that any unenforceable provision will remain as written in any situation other than the situation where the provision held to be unenforceable.

10.7
Dispute Resolution. If a dispute related to this agreement arises, the parties shall attempt to resolve the matter first by mandatory mediation. The party bringing the claim shall include in the notice to the other party a list of at least three mediators who can serve within 120 days of the notice. The party receiving the notice of claim shall choose one mediator from the list provided. The parties shall conduct the mediation within 120 days of the date of the notice. The parties shall divide the fees and costs of the mediator evenly. The parties will attempt to resolve their differences in a mediation lasting at least 16 hours unless the parties resolve the dispute in less time. If at the end of 16 hours or a mutually agreed upon longer period, the parties still have no resolution, each party may use any other remedy or procedure available to it including but not limited to arbitration or litigation.

10.8
Attorneys’ Fees and Costs.
(a) If an arbitration or litigation is initiated by the parties, the prevailing party may collect its reasonable attorney’s fees and costs associated with the Adversarial Proceeding.
(b) The terms of this Section apply to any action for injunctive relief only if a temporary or permanent injunction are entered by a court. The terms of this Section do not apply to the entry of a temporary restraining order.
(c) The terms of this Section do not apply to the attorneys’ fees and costs associated with any mediation initiated under Section "Dispute Resolution", even if the mediation is unsuccessful in resolving the dispute between the parties.

10.9
Choice and Waiver. If a party chooses not to enforce any right granted by this agreement or take any optional course of allowed under this agreement, then that party’s choice does not waive any right or protection for any other time.

10.10
Additional Documents. The parties shall sign or deliver any other documents reasonably necessary to carry out the terms of this agreement.

10.11
Amendments to this Agreement. Any amendment to this Agreement not signed by both parties is void.

10.12
Counterparts. The parties may sign this agreement in multiple counterparts. If the parties sign this agreement in several counterparts, then each counterpart is deemed an original, but all counterparts together will constitute one document.
