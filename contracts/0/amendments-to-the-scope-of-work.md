---
some business days: five business days
---
**Amendments to the Scope of Work.** Client may request amendments to the Scope of Work. Upon receiving a request to amend the Scope of Work, within {{some business days}}, Provider shall provide Client:
(a) The time necessary to make the requested change.
(b) An estimate of additional fees, costs, or expenses that may be charged as a result of the requested change.
(c) A description of how the requested change may affect other services under the Scope of Work, if any.
(d) A description of any changes to this Agreement that may be required by the requested change. 
(e) The parties must agree in writing to any amendments to the Scope of Work before Provider will perform the amendment to the Scope of Work.
