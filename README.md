# Contract Provenance

## Stories

- As a contract generator, I want to pull a standard document and fill in optional terms.

- As a counterparty, I want to see what is boilerplate vs what is specific.

- As a counterparty or reader, I want to see other legal references to the boilerplate or it's derivatives. I also want to see references by others who have used this contract, especially if they're someone I know.

## References

- [Legal Markdown](https://github.com/compleatang/legal-markdown/blob/master/README.md)
- [Ricardian Contracts](https://en.wikipedia.org/wiki/Ricardian_contract) and a [good comparison with blockchain tech](https://101blockchains.com/ricardian-contracts/)
- [Common Paper standard agreements](https://commonpaper.com/standards/)
- [Sample Contracts & Forms](https://www.findlaw.com/smallbusiness/business-contracts-forms/sample-contracts-forms.html)
  - [Historical Corporate & Industry Contracts](https://corporate.findlaw.com/contracts.html)
  - [State-Specific Small Business Forms (for purchase)](https://www.uslegalforms.com/findlaw/) (US Legal won Best Legal Forms from TopTen Reviews)
- Someday: [full governance frameworks](https://github.com/hyperledger/aries-rfcs/tree/main/concepts/0430-machine-readable-governance-frameworks) (and see [a presentation with more extensive data](https://hackmd.io/@mikekebert/HyjBmusEF))